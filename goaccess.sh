#!/bin/sh

# create necessary config dirs if not present
mkdir -p /config/html
mkdir -p /config/data
mkdir -p /opt/log

# copy default goaccess config if not present
[ -f /config/goaccess.conf ] || cp /opt/goaccess.conf /config/goaccess.conf

# make things easier on the users with access to the folders
chmod -R 777 /config

# ready to go
CONFIG="--no-global-config --config-file=/config/goaccess.conf"
FILE=$@

[ ! -z "$FILE" ] || FILE="access"

[ ! -f $FILE.log ] || LOGS="$FILE.log"
[ ! -f $FILE.log.1 ] || LOGS="$LOGS $FILE.log.1"

if [ -z "$LOGS" ]
then
    echo "No file $FILE.log..."
    exit 1
fi

GZ=`find . -name "$FILE.log.*.gz"`

nginx -c /opt/nginx.conf &
if [ -z "$GZ" ]
then
    goaccess $LOGS $CONFIG
else
    zcat $GZ | goaccess $LOGS $CONFIG -
fi
